package com.example.customview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel: ViewModel() {

    private val _pin = MutableLiveData<String>().apply { value = "" }
    val pin: LiveData<String> get() = _pin

    private val _state1 = MutableLiveData<Boolean>().apply { value = false }
    val state1: LiveData<Boolean> get() = _state1

    private val _state2 = MutableLiveData<Boolean>().apply { value = false }
    val state2: LiveData<Boolean> get() = _state2

    private val _state3 = MutableLiveData<Boolean>().apply { value = false }
    val state3: LiveData<Boolean> get() = _state3

    private val _state4 = MutableLiveData<Boolean>().apply { value = false }
    val state4: LiveData<Boolean> get() = _state4

    fun setState1() {
        _state1.value = !state1.value!!
    }

    fun setState2() {
        _state2.value = !_state2.value!!
    }

    fun setState3() {
        _state3.value = !state3.value!!
    }

    fun setState4() {
        _state4.value = !_state4.value!!
    }

    fun setPin(pin: String) {
        _pin.value = pin
    }
}