package com.example.customview

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.*


@SuppressLint("ResourceAsColor", "CustomViewStyleable")
class CustomPin @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) :
    LinearLayout(context, attrs) {

    private val mImageView1: ImageView
    private val mImageView2: ImageView
    private val mImageView3: ImageView
    private val mImageView4: ImageView

    init {
        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.view_custom_pin, this, true)

        mImageView1 = getChildAt(0) as ImageView
        mImageView2 = getChildAt(1) as ImageView
        mImageView3 = getChildAt(2) as ImageView
        mImageView4 = getChildAt(3) as ImageView
    }

    fun setImageView1(state: Boolean) {
        mImageView1.setImageResource(resId(state))
    }
    fun setImageView2(state: Boolean) {
        mImageView2.setImageResource(resId(state))
    }
    fun setImageView3(state: Boolean) {
        mImageView3.setImageResource(resId(state))
    }
    fun setImageView4(state: Boolean) {
        mImageView4.setImageResource(resId(state))
    }

    private fun resId(state: Boolean): Int {
        return if (state)
            R.drawable.ic_circle
        else
            R.drawable.ic_circle_border
    }
}