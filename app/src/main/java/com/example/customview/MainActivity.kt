package com.example.customview

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.activity.addCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.customview.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private val tag = MainActivity::class.java.simpleName
    private val viewModel: MainViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding
    private lateinit var editTextView: EditText
    private lateinit var imm: InputMethodManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDefaultParams()
        setTextListener()
        setOnClick()
        observeViewModels()
    }

    private fun setDefaultParams() {
        binding.animationView.setMaxFrame(13)
        binding.animationView.reverseAnimationSpeed()
    }

    private fun setTextListener() {
        editTextView = findViewById(R.id.editText)
        editTextView.requestFocus()
        editTextView.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                viewModel.setPin(s.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })
        imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editTextView, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun setOnClick() {
        this.onBackPressedDispatcher.addCallback(this) {
            imm.showSoftInput(editTextView, InputMethodManager.SHOW_IMPLICIT)
        }

        binding.customPin.setOnClickListener {
            imm.showSoftInput(editTextView, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    private fun observeViewModels() {
        viewModel.pin.observe(this, Observer { pin ->
            if (pin.length != 4) {
                binding.animationView.apply {
                    if (frame == 13) {
                        reverseAnimationSpeed()
                        playAnimation()
                    }
                }
            }
            when (pin.length) {
                0 -> {
                    if (viewModel.state1.value!!)
                        viewModel.setState1()
                }
                1 -> {
                    if (viewModel.state2.value!!)
                        viewModel.setState2()
                    else
                        viewModel.setState1()
                }
                2 -> {
                    if (viewModel.state3.value!!)
                        viewModel.setState3()
                    else
                        viewModel.setState2()
                }
                3 -> {
                    if (viewModel.state4.value!!)
                        viewModel.setState4()
                    else
                        viewModel.setState3()
                }
                4 -> {
                    viewModel.setState4()
                    binding.animationView.apply {
                        reverseAnimationSpeed()
                        playAnimation()
                    }
                    Log.e(tag, "Pin = ${viewModel.pin.value}")
                    imm.hideSoftInputFromWindow(editTextView.windowToken, 0)
                }
            }
        })

        viewModel.state1.observe(this, Observer { state ->
            binding.customPin.setImageView1(state)
        })
        viewModel.state2.observe(this, Observer { state ->
            binding.customPin.setImageView2(state)
        })
        viewModel.state3.observe(this, Observer { state ->
            binding.customPin.setImageView3(state)
        })
        viewModel.state4.observe(this, Observer { state ->
            binding.customPin.setImageView4(state)
        })
    }
}